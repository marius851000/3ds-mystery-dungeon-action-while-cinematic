$(BUILDDIR)patch/RomFS/script/mods : PMD-Lua-Mod-Loader/RomFS/script/mods newentry.lua
	mkdir -p $(BUILDDIR)patch/RomFS/script/mods
	cp PMD-Lua-Mod-Loader/RomFS/script/mods/* $(BUILDDIR)patch/RomFS/script/mods -r
	cp newentry.lua $(BUILDDIR)patch/RomFS/script/mods/

$(BUILDDIR)patch/RomFS/script/menu : #$(BUILDDIR)patch/RomFS/script/menu/menu_new_dungeon.lua $(BUILDDIR)patch/RomFS/script/menu/menu_dungeon.lua
	mkdir -p $(BUILDDIR)patch/RomFS/script/menu
	cp PMD-Lua-Mod-Loader/RomFS/script/menu/* $(BUILDDIR)patch/RomFS/script/menu

$(BUILDDIR)patch/RomFS/script/menu/menu_new_dungeon.lua : PMD-Lua-Mod-Loader/RomFS/script/menu/menu_new_dungeon.lua
	mkdir -p $(BUILDDIR)patch/RomFS/script/menu
	cp PMD-Lua-Mod-Loader/RomFS/script/menu/menu_new_dungeon.lua $(BUILDDIR)patch/RomFS/script/menu/menu_new_dungeon.lua

$(BUILDDIR)patch/RomFS/script/menu/menu_dungeon.lua : PMD-Lua-Mod-Loader/RomFS/script/menu/menu_dungeon.lua
	mkdir -p $(BUILDDIR)patch/RomFS/script/menu
	cp PMD-Lua-Mod-Loader/RomFS/script/menu/menu_dungeon.lua $(BUILDDIR)patch/RomFS/script/menu/menu_dungeon.lua
