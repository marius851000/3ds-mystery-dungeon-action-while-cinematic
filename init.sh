#/bin/sh

echo "downloading submodule"
git submodule init
git submodule update

echo "making file executable"
chmod +x test.sh compile.sh init.sh
chmod +x compute_file.py configure.py
