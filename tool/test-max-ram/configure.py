initial_size = 1000
max_iteration = 13
max_possible_size = 10000000
base_folder = "./RomFS/script/sizetest/"

all = "all : ./RomFS/script/mods/main.lua "
makefile = """./RomFS/script/mods/main.lua : configure.py
\tmkdir -p ./RomFS/script/mods
\techo 'dofile("script/sizetest/1000.lua")'
"""

actual_iteration = initial_size
for loop in range(max_iteration):
    print("size : "+str(actual_iteration))

    if actual_iteration > max_possible_size:
        raise BaseException("The maximum is "+str(max_possible_size)+".")

    file_loc = base_folder+str(actual_iteration)+".lua"
    makefile += file_loc+" : configure.py\n"
    makefile += "\tmkdir -p "+base_folder+"\n"
    makefile += "\tpython3 generate.py "+file_loc+" "+str(actual_iteration)+" "+base_folder+str(actual_iteration*2)+".lua"+"\n"

    all += " " + file_loc
    actual_iteration *= 2


to_write = all + "\n" + makefile + "\nclean :\n\trm -rf RomFS"

f = open("makefile","w")
f.write(to_write)
f.close()
