#!/bin/python3
import os

script_folder = "PSMD-script/script/event/"
dest_folder = "build/"
tmp_dir = "tmp/"
rom = "./psmd.3ds"
makefile_folder = "./make/"


def parse_make(pos): # now useless
    assert type(pos) == str
    handmade_file = open(pos)
    handmade_text = handmade_file.read()
    handmade_file.close()

    handmade_text = handmade_text.replace("%rom%",rom)
    handmade_text = handmade_text.replace("%BUILDDIR%",dest_folder)
    handmade_text = handmade_text.replace("\\%","%")
    handmade_text += "\n"

    return handmade_text

render = ""
render += """
# auto generated with configure.py
"""

if rom != False:
    render += "ROM = "+rom+"\n"
render += "BUILDDIR = "+dest_folder+"\n"
render += "TMPDIR = "+tmp_dir+"\n"

render += "############################\n"


render += parse_make(makefile_folder+"head.make")

all = "all : "+dest_folder+"patch/RomFS/script/mods "+dest_folder+"patch/RomFS/script/menu"
all_test = "test : "


def concat(liste,chara):
    retour = ""
    for loop in liste:
        retour += loop + chara
    return retour[:-1]

for subdir,dirs,files in os.walk(script_folder):
    for file in files:
        relative_position = concat((subdir+"/"+file).split("/")[1:],"/")
        dest_name = dest_folder+"patch/RomFS/"+relative_position

        render += dest_name+" : "+subdir+"/"+file+" compute_file.py\n"
        render += "\tmkdir -p "+concat((dest_folder+"patch/RomFS/"+relative_position).split("/")[:-1],"/")+"\n"
        render += "\tpython3 compute_file.py "+subdir+"/"+file+" "+dest_folder+"patch/RomFS/"+relative_position+"\n"
        all += " " + dest_name

        # test
        test_dest_name = dest_folder+"patch/RomFS/"+relative_position+"-test"
        render += test_dest_name + ":" + dest_folder+"patch/RomFS/"+relative_position + "\n"
        render += "\tluac -p " + dest_folder+"patch/RomFS/"+relative_position + "\n"
        all_test += " " + test_dest_name



all += "\n"


render += parse_make(makefile_folder+"clean.make")
render += parse_make(makefile_folder+"help.make")
render += parse_make(makefile_folder+"mods.make")
if rom != False:
    render += parse_make(makefile_folder+"rom.make")

make = open("Makefile","w")
make.write("\n")
make.write(all)
make.write("\n")
make.write(all_test)
make.write("\n")
make.write(render)
