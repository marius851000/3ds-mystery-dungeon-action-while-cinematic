$(BUILDDIR)romfs.bin $(BUILDDIR)exefs $(BUILDDIR)exheader.bin : $(ROM)
	mkdir -p $(BUILDDIR)
	mkdir -p $(TMPDIR)
	rm -rf $(TMPDIR)exefs-tmp $(TMPDIR)romfs-tmp $(TMPDIR)exheader-tmp.bin
	ctrtool -x --exefsdir=$(TMPDIR)exefs-tmp --romfs=$(TMPDIR)romfs-tmp.bin --exheader=$(TMPDIR)exheader-tmp.bin $(ROM)
	mv $(TMPDIR)exefs-tmp $(BUILDDIR)exefs
	mv $(TMPDIR)romfs-tmp.bin $(BUILDDIR)romfs.bin
	mv $(TMPDIR)exheader-tmp.bin $(BUILDDIR)exheader.bin
$(BUILDDIR)romfs : $(BUILDDIR)romfs.bin
	mkdir -p $(BUILDDIR) $(TMPDIR)
	rm -rf $(TMPDIR)romfs-tmp
	ctrtool -x --romfsdir=$(TMPDIR)romfs-tmp $(BUILDDIR)romfs.bin
	mv $(TMPDIR)romfs-tmp $(BUILDDIR)romfs
$(BUILDDIR)romfs-modified : $(BUILDDIR)romfs all
	mkdir -p $(BUILDDIR) $(TMPDIR)
	rm -rf $(TMPDIR)romfs-modified-tmp
	cp -rv $(BUILDDIR)romfs $(TMPDIR)romfs-modified-tmp
	cp -rv $(BUILDDIR)patch/RomFS/script/* $(TMPDIR)romfs-modified-tmp/script/
	mv $(TMPDIR)romfs-modified-tmp $(BUILDDIR)romfs-modified
$(BUILDDIR)RSF.rsf : $(ROM) $(BUILDDIR)exheader.bin
	mkdir -p $(BUILDDIR) $(TMPDIR)
	rm -rf $(TMPDIR)RSF-tmp.rsf
	cp 3DS-rom-tools/rsfgen/dummy.rsf $(TMPDIR)RSF-tmp.rsf
	python2 3DS-rom-tools/rsfgen/rsfgen.py -r $(ROM) -e $(BUILDDIR)exheader.bin -o $(TMPDIR)RSF-tmp.rsf
	mv RSF-tmp.rsf $(BUILDDIR)RSF.rsf
$(BUILDDIR)romfs-modified.bin : $(BUILDDIR)romfs-modified
	mkdir -p $(BUILDDIR) $(TMPDIR)
	rm -f $(TMPDIR)romfs-modified-tmp.bin
	wine 3dstool -c -v -t romfs --romfs-dir $(BUILDDIR)romfs-modified -f $(TMPDIR)romfs-modified-tmp.bin
	mv $(TMPDIR)romfs-modified-tmp.bin $(BUILDDIR)romfs-modified.bin
