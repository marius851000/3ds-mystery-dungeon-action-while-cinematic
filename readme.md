freecam while in cinematic for pokémon mystery dungeon on 3ds.

Use the freecam of EddyK28.

to compile all of this by yourself, run compile.sh with bash/sh

You need to have python3 and make installed to be able to compile it.

If you can't have access to a bash/sh shell (cf: you are on windows), install python3 and make, open a command prompt, go to this folder, and run:

python3 configure.py

make all

It'll build the patch in the folder build/patch

You can also search for cywin, intall it with make and python3, and execute compile.sh

Alternatively, you can download precompiled version at https://framagit.org/marius851000/3ds-mystery-dungeon-action-while-cinematic/pipelines ( select the cloud icon on the recentest build, and select download build artifacts ).
Another precompiled version, manualy tested can be found in the root, named "PSMD-pause-tested.tar.xz".



To use this mod, you need to have a hacked 3ds with the luma custom firmware with game patching enabled, and copy the RomFS folder generated in the render folder / extracted from the precompiled file to the folder luma/titles/0004000000174600 for the US version of super or luma/titles/0004000000174400 for the EU version of super.

I haven't actually tested with Gates, but it should work easily.


To use it, you can press L + A while in a story related animation, or while accessing the other menu ( the vanilla version ). Sometime, you wont be able to see the menu in the lower screen ( but you should see the selection cursor ). The freecam is the first in the menu. Press a and you could use the camera. You can turn with the joypad, and advance with x. The other button also do thing. I recommand you to practive with the vanilla method ( L + A while accessing the other menu ), because command are displayed in the lower screen. To quit, press start, select exit ( selected by default ), A and B to exit the mod selection menu.

Please note that the game could crash with this. Remember to save often. I actually try to see the reason (a memory overflow, I suppose).


## TODO:
find why it crash when we move the camera and the cinematic end, and correct this error.
