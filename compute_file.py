#!/bin/python3
import sys

source = sys.argv[1]
dest = sys.argv[2]

debugComment = False
debugOutput = False
chargeBeggining = """function everyfunc()
\tif PAD:Data("L|X") then
\t\tlocal func, err = loadfile("script/mods/newentry.lua")
\t\tlocal stat, err = pcall(func)
\tend
end"""
charge = "everyfunc()"
# It call an external file to reduce the size of the extract, and so to prevent a memory overflow
# chargeBeggining is added to the beggining of the file, charge after (approximatively) every function.
# ne doit pas être inclus dans les fonctions entre crocher

def add_start(line,towhat):
    retour = ""
    for loop in towhat.split("\n"):
        retour += line + loop + "\n"
    return retour[0:-1]

def quote(texte):
    render = '"'
    for loop in texte.split('"'):
        render += loop + '\\"'
    return render[0:-2]+'"'

render = chargeBeggining + "\n"
s = open(source,"r")
line = 0
inCrochet = 0
returnLevel = 0
for loop in s.read().split("\n"):
    line += 1
    #print(line)
    if len(loop) > 0:
        # can fail to check with luac without this
        if loop.split("  ")[-1] == "do break end":
            loop = ""
        if len(loop) > 0:
            if loop[-1] == "{":
                inCrochet += 1
            if loop.split("  ")[-1][0] == "}":
                inCrochet -= 1
            if loop.split("  ")[-1] == "return" or loop.split("  ")[-1][0:7] == "return ":
                returnLevel += 1
            if loop.split("  ")[-1][0:6] in ["end","else","elseif"]:
                returnLevel -= 1
                if returnLevel < 0:
                    returnLevel = 0

    render += loop

    if debugComment:
        render += "  -- l."+str(line)

    render += "\n"

    if inCrochet == 0 and returnLevel == 0:
        if debugOutput:
            render += 'debug_var_azerty = '+quote(str(loop))+'\n'
            render += 'WINDOW:SysMsg(debug_var_azerty:gsub(" ","쐃"))\n'
        render += add_start((len(loop.split("  "))-1)*"  ",charge)+"\n" #TODO: better computation of the tabulation

assert inCrochet == 0
d = open(dest,"w")
d.write(render)
d.close()
