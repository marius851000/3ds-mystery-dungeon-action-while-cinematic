#!/bin/sh

set -e

bash init.sh

echo "configuring"
python3 configure.py
echo "cleaning"
make clean -j$(nproc --all)
echo "making all"
make all -j$(nproc --all)
echo "making test"
make test -j$(nproc --all)


# test tool
echo "testing tool/test-max-ram"
cd tool/test-max-ram
python3 configure.py
make clean
make -j$(nproc --all)
