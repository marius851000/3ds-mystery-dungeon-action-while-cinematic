import sys

output = sys.argv[1]
size = int(sys.argv[2])
next_file = sys.argv[3]


render = """
WINDOW:SysMsg('"""+str(size)+"""')
WINDOW:CloseMessage()
"""

end_file = """
dofile("""+next_file+""")"""

line = "test_max_mem = 1000000\n"

number_line = (size-len(render)-len(end_file))/len(end_file)


for loop in range(int(number_line)):
    render += line

render += end_file


f = open(output,"w")
f.write(render)
f.close()
