#!/bin/sh

set -e

bash init.sh

python3 configure.py
make all -j$(nproc --all)
